

Rails.application.routes.draw do
  resources :shipments
  resources :subscriptions
  get 'plans/show'
  get 'plans/index'

  ##### ROOT
  root 'home#show'

  ##### STATIC PAGES
  get 'static_pages/help', as: 'help'
  get 'static_pages/about', as: 'about'
  get 'static_pages/contact', as: 'contact'
  

  ##### USERS
  resources :users
  get 'signup' => 'users#new'
  resources :account_activations, only: [:edit]


  ##### SESSIONS  
  # Login
  get '/login' => 'sessions#new', as: 'login'
  post '/login' => 'sessions#create', as: 'create_login'

  # Logout
  delete '/logout' => 'sessions#destroy', as: 'logout'


  ##### BOXES + ITEMS3
  get '/boxes/new' => 'boxes#new'
  get '/boxes/:id', to: 'boxes#show'
  
  
  resources :boxes do
    resources :items
  end
  
  ##### ITEMS
  
  get 'items/new' => 'items#new'

  ##### PLANS
  get 'plans/:id' => 'plans#show', as: 'plan'
  get 'plans/' => 'plans#index'

  ##### SUBSCRIPTION MODIFICATION EMANCIPATION PROCLAMATION
  get 'subscriptions/:id/upgrade' => 'subscriptions#upgrade', as: 'upgrade'
  get 'subscriptions/:id/downgrade' => 'subscriptions#downgrade', as: 'downgrade'
  
end
