require 'test_helper'

class UsersLoginTest < ActionDispatch::IntegrationTest

  def setup
    @user = users(:michael)
  end

  test "login with invalid information" do
    get login_path
    assert_template 'sessions/new'
    post login_path, session: { email: "", password: "" }
    assert_template 'sessions/new'
    assert_not flash.empty?
    get root_path
    assert flash.empty?
  end
  
  test "login with valid information, followed by logout" do
    get login_path
    post login_path, session: { email: @user.email, password: 'password' }
    assert is_logged_in? #check and see if test user got logged in
    assert_redirected_to @user
    follow_redirect!
    assert_template 'users/show'
    assert_select "a[href=?]", login_path, count: 0
    assert_select "a[href=?]", logout_path
    assert_select "a[href=?]", user_path(@user)
    delete logout_path  
    assert_not is_logged_in?  #make sure you're not logged in anymore
    assert_redirected_to root_url  #make sure it sends you back to root
    #simulate end user clicking logout in a separate window
    delete logout_path
    follow_redirect!
    assert_select "a[href=?]", login_path
    assert_select "a[href=?]", logout_path,      count: 0
    assert_select "a[href=?]", user_path(@user), count: 0 
  end

  test "login with remembering" do
    log_in_as(@user, remember_me: '1')       #check the box, yo
    assert_not_nil cookies['remember_token'] #make sure this isn't empty
  end
  
  test "login without remembering" do
    log_in_as(@user, remember_me: '0')       #uncheck the box...yo
    assert_nil cookies['remember_token']     #make sure this *is* empty
  end

end