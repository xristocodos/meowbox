require 'test_helper'

class UsersEditTest < ActionDispatch::IntegrationTest
  
  def setup
    @user = users(:michael)
  end
  
  
  test "unsuccessful edit" do
    log_in_as(@user)
    get edit_user_path(@user)     #get user edit page
    assert_template 'users/edit'  #make sure you got user edit page
    patch user_path(@user), user: {   #put in some weird incorrect data
                                    username: "",
                                    first_name: "michael",
                                    last_name: "michaels",
                                    email: "foo@invalid",
                                    password: "foo",
                                    password_confirmation: "bar" 
                                  }
    assert_template 'users/edit'  #make sure it re-served you the same edit page
  end
  
  
  test "successful edit with friendly forwarding" do
    get edit_user_path(@user)
    log_in_as(@user)
    assert_redirected_to edit_user_path(@user)
    # assert_template 'users/edit'
    username  = "Foo Bar"
    first_name = "foo"
    last_name = "bar"
    email = "foo@bar.com"
    patch user_path(@user), user: { username:  username,
                                    first_name: first_name,
                                    last_name: last_name,
                                    email: email,
                                    password:              "",
                                    password_confirmation: "" }
    assert_not flash.empty?
    assert_redirected_to @user
    @user.reload
    assert_equal username,  @user.username    #make sure the username we wrote got into the db obj
    assert_equal first_name, @user.first_name
    assert_equal last_name, @user.last_name
    assert_equal email, @user.email           #make sure the email we wrote got into the db obj
  end
end
