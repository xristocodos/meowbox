require 'test_helper'

class UsersSignupTest < ActionDispatch::IntegrationTest
  def setup
    ActionMailer::Base.deliveries.clear  #clear mail msg delivery count in order to count sents for test
  end


  test "invalid signup information" do
    get signup_path
    assert_no_difference 'User.count' do
      post users_path, user: { username:  "",
                               first_name: "",
                               last_name: "",
                               email: "user@invalid",
                               password:              "foo",
                               password_confirmation: "bar" }
    end
    assert_template 'users/new'
    assert_select 'div#error_explanation' #added 10/9
    assert_select 'div.field_with_errors' #added 10/9
  end
  
  
  test "valid signup information with account activation" do
    get signup_path
    assert_difference 'User.count', 1 do
      post users_path, user:              { username: "KayDawg9009",
                                            first_name: "Kevin",
                                            last_name: "Dawg",
                                            email: "kevin9009@gmail.com",
                                            password: "password5",
                                            password_confirmation: "password5"
                                          }
    end

    assert_equal 1, ActionMailer::Base.deliveries.size #ensures exactly one message is sent
    user = assigns(:user)
    assert_not user.activated?
    
    # case: attempt login before activation.
    log_in_as(user)
    assert_not is_logged_in?
    
    # case: invalid activation token
    get edit_account_activation_path("invalid token", email: user.email)
    assert_not is_logged_in?
    
    # case: Valid token, wrong email
    get edit_account_activation_path(user.activation_token, email: 'wrong')
    assert_not is_logged_in?
    
    # Valid activation token
    get edit_account_activation_path(user.activation_token, email: user.email)
    assert user.reload.activated?
    follow_redirect!
    assert_template 'users/show'
    assert is_logged_in?





    # assert_template 'users/show'
    # assert is_logged_in? #uses test helper method to see if test user got in after all
  end
end