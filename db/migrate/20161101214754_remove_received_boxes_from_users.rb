class RemoveReceivedBoxesFromUsers < ActiveRecord::Migration
  def change
    remove_column :users, :received_boxes, :string
  end
end
