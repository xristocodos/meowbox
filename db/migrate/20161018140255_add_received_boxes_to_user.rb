class AddReceivedBoxesToUser < ActiveRecord::Migration
  def change
    add_column :users, :received_boxes, :string
  end
end