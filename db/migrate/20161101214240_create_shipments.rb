class CreateShipments < ActiveRecord::Migration
  def change
    create_table :shipments do |t|
      t.integer :user_id
      t.integer :box_id

      t.timestamps null: false
    end
  end
end
