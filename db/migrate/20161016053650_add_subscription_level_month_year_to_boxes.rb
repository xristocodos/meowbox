class AddSubscriptionLevelMonthYearToBoxes < ActiveRecord::Migration
  def change
    add_column :boxes, :subscription_level, :integer
    add_column :boxes, :month, :integer
    add_column :boxes, :year, :integer
  end
end
