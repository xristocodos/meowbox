class CreateItems < ActiveRecord::Migration
  def change
    create_table :items do |t|
      t.string :title
      t.text :description
      t.string :image_url
      t.string :size
      t.string :buy_more_url
      t.timestamps null: false
    end
  end
end
