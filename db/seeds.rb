# This file should contain all the record creation needed to seed the database with its default values.
# The data can then be loaded with the rake db:seed (or created alongside the db with db:setup).
#
# Examples:
#
#   cities = City.create([{ name: 'Chicago' }, { name: 'Copenhagen' }])
#   Mayor.create(name: 'Emanuel', city: cities.first)


# This file should contain all the record creation needed to seed the database with its default values.
# The data can then be loaded with the rake db:seed (or created alongside the db with db:setup).

###### CREATE SUBSCRIBER
admin1 = User.create(
  username: "admin", 
  email: "admin@meowbox.com", 
  first_name: "Admin",
  last_name: "McAdministratorson",
  password: "password",
  password_confirmation: "password",
  activated: true,
  is_admin: true
)

subscriber1 = User.create(
  username: "xristocatopolous", 
  email: "xristo@xris.to", 
  first_name: "Xris",
  last_name: "Toast",
  password: "password",
  password_confirmation: "password",
  activated: true
)
  
subscriber2 = User.create(
  username: "bridgeb0ss",
  email: "picardjl@starfleet.ufp.gov",
  first_name: "Jean Luc",
  last_name: "Picard",
  password: "password",
  password_confirmation: "password"
)

subscriber3 = User.create(
  username: "c4tl4dy",
  email: "h1146644@mvrht.com",
  first_name: "Cat",
  last_name: "Lady",
  password: "password",
  password_confirmation: "password"
)
  
  
###### CREATE A WHOLE MESSAMESSERSCHMIDTS VIA FAKER
User.create!(username:              "examply",
             email:                 "example@example.net",
             first_name:            "Example",
             last_name:             "User",
             password:              "foobar",
             password_confirmation: "foobar",
             is_admin:              true,
             activated:             true,
             activated_at:          Time.zone.now)
             

99.times do |n|
  username  = Faker::Name.name
  email = "example-#{n+1}@example.net"
  first_name = "Example"
  last_name = "User"
  password = "password"
  User.create!(username:              username,
               email:                 email,
               first_name:            first_name,
               last_name:             last_name,
               password:              password,
               password_confirmation: password, 
               activated:             true,
               activated_at:          Time.zone.now)
end


###### CREATE PLAN TIERS
plan10 = Plan.create(name: "Berlioz", price: 10, description: "This entry-level package offers value worthy of ap-paws! Comes with themed outfits (New Years, February, Thanksgiving, and Christmas). Great for first time users.")                                                                                
plan30 = Plan.create(name: "Marie", price: 30, description: "This package comes with thematic outfits, and treats/food for that special feline in your family!")
plan100 = Plan.create(name: "Toulouse", price: 100, description: "This deluxe package contains literally ALL the bells & whistles. This includes only the most elaborate outfits, replete with accessories, enough food/treats to accomodate any potential feline guests,  Also an extra special surprise toy in every package!")


###### CREATE SUBSCRIPTIONS (JOIN SUBSCRIBER WITH PLAN ?with extra field?)
subscription1 = Subscription.create(user_id: 1, plan_id: 1)
subscription2 = Subscription.create(user_id: 2, plan_id: 2)
subscription3 = Subscription.create(user_id: 3, plan_id: 3)
subscription4 = Subscription.create(user_id: 4, plan_id: 1)
subscription5 = Subscription.create(user_id: 4, plan_id: 2)





###### CREATE BOX (SEE FOR INFO)
box1sml = Box.create(theme: "Berlioz is the Hallow's Meow", subscription_level: 1, month: 10, year: 2016)
box1med = Box.create(theme: "Marie is the Hallow's Meow", subscription_level: 2, month: 10, year: 2016)
box1lrg = Box.create(theme: "Toulouse is the Hallow's Meow", subscription_level: 3, month: 10, year: 2016)

box2sml = Box.create(theme: "Murry Catsmas, Berlioz!", subscription_level: 1, month: 12, year: 2016)
box2med = Box.create(theme: "Murry Catsmas, Marie!", subscription_level: 2, month: 12, year: 2016)
box2lrg = Box.create(theme: "Murry Catsmas, Toulouse!", subscription_level: 3, month: 12, year: 2016)

box3sml = Box.create(theme: "Happy Nyan Year, Berlioz!", subscription_level: 1, month: 1, year: 2017)
box3med = Box.create(theme: "Happy Nyan Year, Marie!", subscription_level: 2, month: 1, year: 2017)
box3lrg = Box.create(theme: "Happy Nyan Year, Toulouse!", subscription_level: 3, month: 1, year: 2017)

box4sml = Box.create(theme: "Berlioz has February Cabin Fever", subscription_level: 1, month: 2, year: 2017)
box4med = Box.create(theme: "Marie has February Cabin Fever", subscription_level: 2, month: 2, year: 2017)
box4lrg = Box.create(theme: "Toulouse has February Cabin Fever", subscription_level: 3, month: 2, year: 2017)



###### CREATE ITEMS
  ##OUTFITS
item_outfit1 = Item.create(
  title: "Lion's Mane",
  description: "tell your cat to quit lion this Halloween, with this somewhat ironic costume allowing him or her to sidestep evolution and follow its dreams of being a predator in the Serengeti that is your living room. Catching mice is no longer a task, but an epic adventure relying on years of buried instinct and prowess!",
  image_url: "https://images-na.ssl-images-amazon.com/images/I/51LNt0awaLL.jpg",
  size: "2 inches",
  buy_more_url: "https://www.amazon.com/Dogloveit-Costume-Halloween-products-sellers/dp/B00NW8QEDY"
)

item_outfit2 = Item.create(
  title: "Cat Santa Hat",
  description: "The Grinch meets Cat in the Hat! Imagine the possibilities. Also, no presents, sorry, bc: no opposable thumbs",
  image_url: "https://images-na.ssl-images-amazon.com/images/I/41aQs7mj35L.jpg",
  size: "10 picometers",
  buy_more_url: "https://www.amazon.com/Pet-Leso-Christmas-Muffler-Puppy/dp/B017LXTSDI/"
)

item_outfit3 = Item.create(
  title: "Nyancat Outfit",
  description: "Why SHOULDNT your cat be a poptart with rainbows of light for excrement, come this new year?",
  image_url: "https://s-media-cache-ak0.pinimg.com/564x/b6/a2/00/b6a200911010ec3a6072bb294a09f93b.jpg",
  size: "a hundred orlats",
  buy_more_url: "https://www.google.com"
)

item_outfit4 = Item.create(
  title: "Baby BlueTurtleneck Sweater",
  description: "Stay warm outside and listen to Action Bronson's Baby Blue while rocking this adorable knit sweater!",
  image_url: "https://images-na.ssl-images-amazon.com/images/I/71F0-SEyiLL._SL1000_.jpg",
  size: "a hundred orlats",
  buy_more_url: "https://www.amazon.com/dp/B00ZZATQ64/ref=cm_sw_r_cp_api_-qmbybDV611XE"
)



  ##TREATS
item_treat1 = Item.create(
  title: "Steak Treats",
  description: "PRIMAL treats for that lion in your domicile, where your cat used to be",
  image_url: "https://images-na.ssl-images-amazon.com/images/I/71n29VHgg-L._SL1000_.jpg",
  size: "5.29oz x 10",
  buy_more_url: "https://www.amazon.com/TEMPTATIONS-Shoppe-Middles-Treats-Flavors/dp/B00OLSBRTU"
)

item_treat2 = Item.create(
  title: "Freeze Dried Single-Ingredient Turkey Treats",
  description: "Celebrate the holidays with turkey all around!",
  image_url: "https://images-na.ssl-images-amazon.com/images/I/71fyk6TEFAL._SL1478_.jpg",
  size: "1oz",
  buy_more_url: "https://www.amazon.com/dp/B007THO8G4/ref=cm_sw_r_cp_api_qBmbyb6K589ZD"
)

item_treat3 = Item.create(
  title: "Chicken & Turkey Treats",
  description: "Chicken & Turkey Lean protein time, to combat the winter weight gain proactively",
  image_url: "https://images-na.ssl-images-amazon.com/images/I/811FVp1O2UL._SL1500_.jpg",
  size: "2oz",
  buy_more_url: "https://www.amazon.com/dp/B009NOV1A2/ref=cm_sw_r_cp_api_aAmbybTTRY45V"
)

item_treat4 = Item.create(
  title: "Chicken & Trout Treats",
  description: "Chicken & Trout, so your feline friend can fantasize about catching fish after the rivers thaw :)",
  image_url: "https://images-na.ssl-images-amazon.com/images/I/81xMqAKfdwL._SL1500_.jpg",
  size: "2oz",
  buy_more_url: "https://www.amazon.com/dp/B0096S9TM2/ref=cm_sw_r_cp_api_etmbyb2SKWR74"
)

  
  ##TOYS
  
item_toy1 = Item.create(
  title: "Scratchy McScratchpost", 
  description: "Better that Simba scratches this than your new leather sofa...amirite?", 
  image_url: "https://images-na.ssl-images-amazon.com/images/G/01/AXB_pets/Pioneer_SmartCat/Ultimate_Post_wCat._V402326037_.jpg", 
  size: "27 cubits", 
  buy_more_url: "https://www.amazon.com/SmartCat-3832-Ultimate-Scratching-Post/dp/B000634MH8"
)

item_toy2 = Item.create(
  title: "SmartyKat Skitter Critters Cat Toy Catnip Mice", 
  description: "Not a creature was stirring, not even a CATNIP mouse, if your pet has anything to say about it!", 
  image_url: "https://images-na.ssl-images-amazon.com/images/I/710iEpCvmbL._SL1500_.jpg", 
  size: "3-pack", 
  buy_more_url: "https://www.amazon.com/dp/B001FK4744/ref=cm_sw_r_cp_api_BCmbybBQKX852"
)

item_toy3 = Item.create(
  title: "Cat Dancer", 
  description: "Cat Charmer interactive cat toy! So Mr Bigglesworth can finally get ready for tryouts for the color guard!", 
  image_url: "https://images-na.ssl-images-amazon.com/images/I/71k8vsnI2RL._SL1058_.jpg", 
  size: "1 toy", 
  buy_more_url: "https://www.amazon.com/dp/B0002DHV16/ref=cm_sw_r_cp_api_5EmbybFDBJ4NF"
)

item_toy4 = Item.create(
  title: "Turbo Scratcher", 
  description: "Beat Cabin Fever with this fully scr-scr-scratchable toy, complete with trapped rollingball!", 
  image_url: "https://images-na.ssl-images-amazon.com/images/I/91kDN7yvZRL._SL1500_.jpg", 
  size: "1 disc", 
  buy_more_url: "https://www.amazon.com/dp/B000IYSAIW/ref=cm_sw_r_cp_api_HDmbyb6MYBJ5C"
)


### CREATE BOX_CONFIGs (JOIN BOX WITH ITEM WITH EXTRA qty)
#SML GETS OUTFIT, MED GETS OUTFIT TREAT, LRG GETS OUTFIT TREAT TOY
###HALLOWEEN
boxitem1 = BoxItem.create(box_id: box1sml.id, item_id: item_outfit1.id, item_qty: 1)

boxitem2 = BoxItem.create(box_id: box1med.id, item_id: item_outfit1.id, item_qty: 1)
boxitem3 = BoxItem.create(box_id: box1med.id, item_id: item_treat1.id, item_qty: 1)

boxitem4 = BoxItem.create(box_id: box1lrg.id, item_id: item_outfit1.id, item_qty: 1)
boxitem5 = BoxItem.create(box_id: box1lrg.id, item_id: item_treat1.id, item_qty: 1)
boxitem6 = BoxItem.create(box_id: box1lrg.id, item_id: item_toy1.id, item_qty: 1)

###CHRISTMAS
boxitem7 = BoxItem.create(box_id: box2sml.id, item_id: item_outfit2.id, item_qty: 1)

boxitem8 = BoxItem.create(box_id: box2med.id, item_id: item_outfit2.id, item_qty: 1)
boxitem9 = BoxItem.create(box_id: box2med.id, item_id: item_treat2.id, item_qty: 1)

boxitem10 = BoxItem.create(box_id: box2lrg.id, item_id: item_outfit2.id, item_qty: 1)
boxitem11 = BoxItem.create(box_id: box2lrg.id, item_id: item_treat2.id, item_qty: 1)
boxitem12 = BoxItem.create(box_id: box2lrg.id, item_id: item_toy2.id, item_qty: 1)

###NEW YEARS EVE
boxitem13 = BoxItem.create(box_id: box3sml.id, item_id: item_outfit3.id, item_qty: 1)

boxitem14 = BoxItem.create(box_id: box3med.id, item_id: item_outfit3.id, item_qty: 1)
boxitem15 = BoxItem.create(box_id: box3med.id, item_id: item_treat3.id, item_qty: 1)

boxitem16 = BoxItem.create(box_id: box3lrg.id, item_id: item_outfit3.id, item_qty: 1)
boxitem17 = BoxItem.create(box_id: box3lrg.id, item_id: item_treat3.id, item_qty: 1)
boxitem18 = BoxItem.create(box_id: box3lrg.id, item_id: item_toy3.id, item_qty: 1)

###FEBRUARY
boxitem19 = BoxItem.create(box_id: box4sml.id, item_id: item_outfit4.id, item_qty: 1)

boxitem20 = BoxItem.create(box_id: box4med.id, item_id: item_outfit4.id, item_qty: 1)
boxitem21 = BoxItem.create(box_id: box4med.id, item_id: item_treat4.id, item_qty: 1)

boxitem22 = BoxItem.create(box_id: box4lrg.id, item_id: item_outfit4.id, item_qty: 1)
boxitem23 = BoxItem.create(box_id: box4lrg.id, item_id: item_treat4.id, item_qty: 1)
boxitem24 = BoxItem.create(box_id: box4lrg.id, item_id: item_toy4.id, item_qty: 1)


###SHIPMENTS
shipment1 = Shipment.create(user_id: 4, box_id: 1)
shipment2 = Shipment.create(user_id: 4, box_id: 7)