class Box < ActiveRecord::Base
  validates :theme, :presence => true, :length => { :minimum => 2 }
  validates :subscription_level,  :presence => true
  validates :month, :presence => true
  validates :year, :presence => true

  has_many :box_items
  has_many :items, through: :box_items
  
  accepts_nested_attributes_for :items
  
  # def items_attributes=(item_attributes)
  #   item_attributes.values.each do |item_attribute|
  #     item = Item.find_or_create_by(item_attribute)
  #     self.items << item
  #   end
  # end
end