class Item < ActiveRecord::Base
  has_many :box_items
  has_many :boxes, through: :box_items
end
