class User < ActiveRecord::Base
  attr_accessor :remember_token, :activation_token #for tokens, obvies
  before_save   :downcase_email
  before_create :create_activation_digest
  
  has_many :subscriptions
  has_many :plans, through: :subscriptions
  
  has_many :shipments
  has_many :boxes, through: :shipments

  #################### CALLBACKS ####################
  before_save { self.email = email.downcase }

  #################### VALIDATIONS ####################
  VALID_EMAIL_REGEX = /\A[\w+\-.]+@[a-z\d\-.]+\.[a-z]+\z/i
  validates :username, presence: true, length: { maximum: 50 }
  
  validates :first_name, presence: true
  validates :last_name, presence: true

  validates :email, 
    presence: true, 
    length: { maximum: 255 },
    format: { with: VALID_EMAIL_REGEX },
    uniqueness: { case_sensitive: false }

  #################### pw handling ####################  
  has_secure_password
  validates :password, presence: true, length: { minimum: 6 }, allow_nil: true
  #allow_nil is ok bc has_secure_password has a separate validation system
  
  #################### other ####################    
  def User.digest(string)
    #switch to decide whatever value populates the computational cost variable, then put it in
    cost = ActiveModel::SecurePassword.min_cost ? BCrypt::Engine::MIN_COST : BCrypt::Engine.cost
    BCrypt::Password.create(string, cost: cost)
  end


  #rtn random token
  def User.new_token
    SecureRandom.urlsafe_base64
  end
  
  
  def remember  #for actuating "remember me"
    self.remember_token = User.new_token
    update_attribute(:remember_digest, User.digest(remember_token))
  end

  

  # Returns true if the given token matches the digest.
  def authenticated?(attribute, token)
    digest = send("#{attribute}_digest")
    return false if digest.nil?
    BCrypt::Password.new(digest).is_password?(token)
  end
   
   
  #forget that usey mcuser
  def forget #called by sessions helper
    update_attribute(:remember_digest, nil)
  end
  
  
  private

    # Converts email to all lower-case.
    def downcase_email
      self.email = email.downcase
    end

    # Creates and assigns the activation token and digest.
    def create_activation_digest
      self.activation_token  = User.new_token
      self.activation_digest = User.digest(activation_token)
    end  
  
  
  
end
