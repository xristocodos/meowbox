class BoxesController < ApplicationController
  before_action :logged_in_user, only: [:index, :new, :create, :edit, :update, :destroy]  #make sure logged in to see these pgs
  before_action :admin_user,     only: [:index, :new, :create, :edit, :update, :destroy]
  
  def index
    #show all boxes statuses
    @boxes = Box.paginate(page: params[:page])
  end


  def show
    @user = current_user #for conditional "ship this box" button for admins
    @box = Box.find(params[:id])
    @plan = Plan.find(@box.subscription_level)
    @items = @box.items
  end


  def new
    ##build out objects to provide to form
    @box = Box.new
    
    3.times do
      @box.items << Item.new
    end
  end


  def create

    @box = Box.new
    @box.theme = params[:box][:theme]
    @box.subscription_level = params[:box][:subscription_level]
    @box.month = params[:box][:month]
    @box.year = params[:box][:year]
    
    # I KNOOOOOOOWWW THERES A MUCH BETTER WAY TO DO THIS IN ONE LINE BUT I JUST WANT IT WORKING FIRST
    (0..2).each do |num|
      if params[:box][:items_attributes][num.to_s][:title]
        @box.items << Item.new
        @box.items[num].title =         params[:box][:items_attributes][num.to_s][:title]
        @box.items[num].description =   params[:box][:items_attributes][num.to_s][:description]
        @box.items[num].image_url =     params[:box][:items_attributes][num.to_s][:image_url]
        @box.items[num].size =          params[:box][:items_attributes][num.to_s][:size]
        @box.items[num].buy_more_url =  params[:box][:items_attributes][num.to_s][:buy_more_url]
        #get this working then try @box.items[0].items_attributes = 
      end
    end

    if @box.save
      flash[:info] = "Box created!"
      redirect_to @box
    else #if writing the user to the db just is not happening that day...reload form
      flash[:warning] = "Oops, you missed a spot!"
      render 'new'
    end
  end


  def edit
    @box = Box.find(params[:id])
  end
  
  
  def update
  end


  def destroy
    Box.find(params[:id]).destroy
    flash[:success] = "Box deleted"
    redirect_to boxes_url
  end

  
  
  private
    # def box_params
    #   params.require(:box).permit(:theme, item_ids:[], item_attributes: [:title, :description, :image_url, :size, :buy_more_url])
    # end
    
    ###before filters
    def logged_in_user
      unless logged_in?
        store_location
        flash[:danger] = "Uh, quick paws: You need to be logged in to do that!"
        redirect_to login_url
      end
    end
    
    # Confirms user adminnyness
    def admin_user
      # unless @current_user.is_admin
      #   flash[:danger] = "Sorry, only Administrator accounts wield teh powerz teh create/modify/delete box3n!"
      #   redirect_to(box_url) unless @current_user.is_admin
      # end
    end
  
  
end
