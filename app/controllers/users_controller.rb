class UsersController < ApplicationController
  before_action :logged_in_user, only: [:index, :edit, :update, :destroy]  #make sure logged in to see these pgs
  before_action :correct_user,   only: [:edit, :update]  #make sure user A can't edit user B
  before_action :admin_user,     only: :destroy
  
  def index
    @users = User.paginate(page: params[:page])
  end


  def show
    @user = User.find(params[:id])
    @user_subs = @user.subscriptions
    
    #Plan.find(myUser.subscriptions.first.plan_id).name
  end


  def new
    @user = User.new
  end


  def create
    @user = User.new(user_params)
    if @user.save
      UserMailer.account_activation(@user).deliver_now
      flash[:info] = "Please check your email to activate your account."
      redirect_to root_url
    else #if writing the user to the db just is not happening that day...reload form
      render 'new'
    end
  end


  def edit
    #binding.pry
    @user = User.find(params[:id])
  end


  def update
    @user = User.find(params[:id])
    if @user.update_attributes(user_params)
      #handle successfulness of your update here
      flash[:success] = "Your profile is updated! Purrfect!"
      redirect_to @user
    else
      render 'edit'
    end
  end


  def destroy
    User.find(params[:id]).destroy
    flash[:success] = "User deleted"
    redirect_to users_url
  end
  
  
############  
  private
    def user_params
      #binding.pry
      params.require(:user).permit(:username, :first_name, :last_name, :email, :password, :password_confirmation)
    end
    
    ###before filters
    def logged_in_user
      unless logged_in?
        store_location
        flash[:danger] = "Uh, quick paws: You need to be logged in to do that!"
        redirect_to login_url
      end
    end
    
    # Confirms the correct user.
    def correct_user
      @user = User.find(params[:id])
      redirect_to(root_url) unless current_user?(@user)
    end
    
    # Confirms user adminnyness
    def admin_user
      redirect_to(root_url) unless current_user.is_admin?
    end
end
