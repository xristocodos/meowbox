class ShipmentsController < ApplicationController
  def new

    #1. get info we need (box id, subscription level of box)
    box_id = params[:box_id]
    box_plan = params[:box_plan]

    #2. get list of all subscribers from that subscription level
    #3. for every subscriber, create a new shipment with box.id and that subscriber's id
    Subscription.where(plan_id: box_plan).find_each do |scription|
      Shipment.create(user_id: scription.user_id, box_id: box_id)
    end

    #4. redirect to box show with 'package on its way' message
    flash[:success] = "That box is on its merry way!"
    redirect_to boxes_url
  end
  
  def index
    @shipments = Shipment.paginate(page: params[:page])
  end
  
  def destroy
    Shipment.find(params[:id]).destroy
    flash[:success] = "Shipping record erased"
    redirect_to shipments_url
  end

end
