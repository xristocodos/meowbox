class ItemsController < ApplicationController
  #before_action :admin_user,     only: [:new, :create, :edit, :update, :destroy]


  def index
    @items = Item.paginate(page: params[:page])
  end


  def show
    @item = Item.find(params[:id])
    #debugger #brings up byebug in the console
  end


  def new
    if params(:box_id) != nil
      @box = Box.find(params[:box_id])
      @item = Item.new
    else
      #if there is no box
    end
  end


  def create
    @box = Box.find(params[:box_id])
    @item = @box.items.build(params[:items])

    if @item.save 
      flash[:notice] = "Item created"
      redirect_to @item.box
    else
      flash[:error] = "Could not add tag at this time"
      redirect_to @item.box
    end
  end
  
  #  textbook create method
  # def create
  #   @post = Post.new
  #   @post.title = params[:title]
  #   @post.description = params[:description]
  #   @post.save
  #   redirect_to post_path(@post)
  # end





  def edit
    #binding.pry
    @item = Item.find(params[:id])
  end
  
  
  def update
    binding.pry
  end
  
  
 private
    def item_params
      params.require(:item)
    end
end
