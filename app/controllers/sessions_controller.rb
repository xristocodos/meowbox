class SessionsController < ApplicationController
  def new
  end

  def create
    #binding.pry
    user = User.find_by(email: params[:session][:email].downcase)
    if user && user.authenticate(params[:session][:password]) #if it was found & it auth-checks-out
      if user.activated?  #if user activated, log them in, check for remember-me, take care of it, then redirect to userpage
        log_in user
        params[:session][:remember_me] == '1' ? remember(user) : forget(user)
        redirect_back_or user
      else
        message  = "Account not activated, you clumsy cat! "
        message += "Check your email for the activation link."
        flash[:warning] = message
        redirect_to root_url
      end
    end
  end

  def destroy
    log_out if logged_in? #see sessions helper
    redirect_to root_url #send back to root
  end
end
