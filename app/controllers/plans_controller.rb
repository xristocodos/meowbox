class PlansController < ApplicationController
  def show
    @plan = Plan.find(params[:id])
  end

  def index
    @plans = Plan.all
  end
  
end
