module SessionsHelper
  #logs in current user
  def log_in(user)
    session[:user_id] = user.id
  end

  
  #remembers users in a persistent session
  def remember(user)
    user.remember
    cookies.permanent.signed[:user_id] = user.id
    cookies.permanent[:remember_token] = user.remember_token
  end


  #return bool whether user in question is current user
  def current_user?(user)
    user == current_user
  end
  

  #return current logged in user (if there is one)
  def current_user
    if (user_id = session[:user_id])
      @current_user ||= User.find_by(id: user_id)
    elsif (user_id = cookies.signed[:user_id])
      user = User.find_by(id: user_id)
      if user && user.authenticated?(:remember, cookies[:remember_token])
        log_in user
        @current_user = user
      end
    end
  end

  
  # Returns true if the user is logged in, false otherwise.
  def logged_in?
    !current_user.nil?
  end
  
  def forget(user)
    user.forget #calls model method to erase remember_digest
    cookies.delete(:user_id)  #delete userid token
    cookies.delete(:remember_token) #delete remebering token
  end
  
  # Logs the current user back out
  def log_out
    forget(current_user)
    session.delete(:user_id)
    @current_user = nil
  end
  
  
  # redirect to stored location (or default)
  def redirect_back_or(default)
    redirect_to(session[:forwarding_url] || default)
    session.delete(:forwarding_url)
  end
  
  # Store url that's being accessed (or attempted anyway)
  def store_location
    session[:forwarding_url] = request.url if request.get?
  end
end
